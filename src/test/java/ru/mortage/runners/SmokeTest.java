package ru.mortage.runners;

import com.codeborne.selenide.Configuration;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Cucumber.class)

@CucumberOptions(
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json"},
        features = "src/test/java/ru/mortage/features",
        glue = "ru/mortage/steps",
        tags = "@001")

public class SmokeTest {
    static long startTime;

    @BeforeClass
    static public void setupTimeout() {
        Configuration.timeout = 10000;
//        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
//        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
//
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
//        Configuration.browserSize = "1920x1080";

    }


}

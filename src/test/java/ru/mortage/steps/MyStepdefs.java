package ru.mortage.steps;


import com.codeborne.selenide.Condition;
import cucumber.api.PendingException;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;



public class MyStepdefs {
    @Когда("^пользователь заходит на страницу \"([^\"]*)\"$")
    public void пользовательЗаходитНаСтраницу(String url) throws Throwable {
        open(url);
        System.out.println("перешли на нужную страницу");
    }

    @Когда("^пользователь нажимает на кнопку с id \"([^\"]*)\"$")
    public void пользовательНажимаетНаКнопкуСId(String id) throws Throwable {
        $(By.id(id)).click();
    }

    @Тогда("^пользователь видит модальное окно с текстом \"([^\"]*)\"$")
    public void пользовательВидитМодальноеОкноСТекстом(String text) throws Throwable {
        $(By.xpath(".//div[@data-testid='modal']")).$(By.xpath(".//*[text()='" + text + "']")).has(Condition.exist);
    }

    @Когда("^пользователь вводит текст \"([^\"]*)\" в поле с id \"([^\"]*)\"$")
    public void пользовательВводитТекстВПолеСId(String text, String id) throws Throwable {
        $(By.id(id)).setValue(text);
    }

    @Тогда("^пользователь переходит на страницу \"([^\"]*)\"$")
    public void пользовательПереходитНаСтраницу(String text) throws Throwable {
        $(By.id("title")).shouldBe(Condition.text(text));
    }
}

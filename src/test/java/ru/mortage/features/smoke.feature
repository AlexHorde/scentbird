# language: ru

@001
Функция: Создания заявки


  Сценарий: смоук тест


    Когда пользователь заходит на страницу "https://www.scentbird.com/gift"
    Когда пользователь нажимает на кнопку с id "buyGiftNow"
    Когда пользователь нажимает на кнопку с id "buyForHim"
    Тогда пользователь видит модальное окно с текстом "CHOOSE THE SIZE OF YOUR GIFT"

    Когда пользователь нажимает на кнопку с id "giftSize2Button"
    Тогда пользователь видит модальное окно с текстом "WHO IS THIS GIFT FOR?"

    Когда пользователь вводит текст "alex" в поле с id "giftModalRecipientName"
    Когда пользователь вводит текст "aktolpekin@yandex.ru" в поле с id "giftModalRecipientEmail"
    Когда пользователь вводит текст "test" в поле с id "giftModalPersonalMessage"
    Когда пользователь нажимает на кнопку с id "giftPersonNextButton"
    Тогда пользователь видит модальное окно с текстом "WHEN DO YOU WANT TO NOTIFY THE RECIPIENT OF YOUR GIFT?"

    Когда пользователь нажимает на кнопку с id "giftRightNowButton"
    Тогда пользователь переходит на страницу "SECURE CHECKOUT"